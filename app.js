const axios = require("axios");
const express = require("express");
const cors = require("cors");
const nodemailer = require("nodemailer");
require("dotenv").config();
// Create Express Server
const app = express();
app.use(express.json());
app.use(cors());

// Configuration
const PORT = 3000;
const accessKey = "u$r31d0b20fec434a1145f2f3cf79d0d107";
const secretKey = "6f16fe8eabe292a34445b605607a8aab8c065a87";

app.post("/sms", async function (req, res) {
  try {
    await axios
      .post(
        "https://exoverify.exotel.com/v2/accounts/housr1/verifications/sms",
        req.body,
        {
          auth: {
            username: "30dda85c0802315dcccb9bd1f29e16c5",
            password: "hadotejovaqi",
          },
        }
      )
      .then((response) => {
        // return the response from the destination
        res.json(response.data);
      })
      .catch((error) => {
        // handle any errors
        res.send(error);
      });
  } catch (e) {
    return res.status(404).json(e);
  }
});

app.post("/verifysms/:verificationid", async function (req, res) {
  try {
    await axios
      .post(
        `https://exoverify.exotel.com/v2/accounts/housr1/verifications/sms/${req.params.verificationid}`,
        req.body,
        {
          auth: {
            username: "30dda85c0802315dcccb9bd1f29e16c5",
            password: "hadotejovaqi",
          },
        }
      )
      .then((response) => {
        // return the response from the destination
        res.json(response.data);
      })
      .catch((error) => {
        // handle any errors
        res.send(error);
      });
  } catch (e) {
    return res.status(404).json({ status: "error" });
  }
});

app.post("/api/v1/sendemail", async function (req, res) {
  const { subject, text } = req.body;

  let mailTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "neeraj.yadav@housr.in",
      pass: "Housr@123",
    },
  });

  let details = {
    from: "neeraj.yadav@housr.in",
    to: "hr@housr.in",
    subject: subject,
    text: text,
  };

  mailTransport.sendMail(details, (err) => {
    if (err) {
      // console.log(err);
    } else {
      // console.log("SuccessFull!", res);
      return res.json("Successfull");
    }
  });
});

app.post("/api/v1/leadsquared/lead-create", async function (req, res) {
  try {
    const response = await axios.post(
      `https://api-in21.leadsquared.com/v2/LeadManagement.svc/Lead.Capture?accessKey=${accessKey}&secretKey=${secretKey}`,
      req.body,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    res.status(200).send(response.data);
  } catch (error) {
    console.error(error);
    res.status(500).send("Error creating lead");
  }
});

app.post("/api/v1/leadsquared/notify-user", async function (req, res) {
  try {
    const response = await axios.post(
      `https://api-in21.leadsquared.com/v2/UserManagemenwt.svc/NotifyUsers?accessKey=${accessKey}&secretKey=${secretKey}`,
      req.body,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    res.status(200).send(response.data);
  } catch (error) {
    console.error(error);
    res.status(500).send("Error creating lead");
  }
});

// Starting our Proxy server
app.listen(PORT, () => {
  console.log(`Starting Proxy:${PORT}`);
});
